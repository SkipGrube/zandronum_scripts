#!/bin/bash

# Very simple script to save the contents of a "screenlog.0" file in a given directory to a subdirectory
# We run this when we restart the doom servers to have a record of what happened on the server that week

LOGDIR=$1
TODAY=`date +"%Y%m%d"`

mkdir -p ${LOGDIR}/screenlogs

mv ${LOGDIR}/screenlog.0 ${LOGDIR}/screenlogs/screenlog.0.${TODAY}
touch ${LOGDIR}/screenlog.0
