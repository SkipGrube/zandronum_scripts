#!/bin/bash

SERVER=~/bin/zandronum-server
WAD_DIR=~/doom_server

# NOTE: be sure to start this script after invoking "screen -S doom -L", as that will log the output



# Here's the real server boot, with these options:
# -Ultra-violence difficulty (skill 4)
# -no monster respawn
# -no item respawn
# -players get infinite lives
# -keys shared between players
# -no jumping/crouching
# -players can walk through each other
#
# -E7M1 (Spear of Destiny lvl 1) is disabled, as the spawns at the start are broken
#

${SERVER} -iwad ${WAD_DIR}/doom2.wad -port 11666 +sv_maxlives 0 -host +alwaysapplydmflags 1 -skill 4 +cooperative 1 +map MAP01 +addmap MAP01 +addmap MAP02 +addmap MAP03 +addmap MAP04 +addmap MAP05 +addmap MAP06 +addmap MAP07 +addmap MAP08 +addmap MAP09 +addmap MAP10 +addmap MAP11 +addmap MAP12 +addmap MAP13 +addmap MAP14 +addmap MAP15 +addmap MAP16 +addmap MAP17 +addmap MAP18 +addmap MAP19 +addmap MAP20 +addmap MAP21 +addmap MAP22 +addmap MAP23 +addmap MAP24 +addmap MAP25 +addmap MAP26 +addmap MAP27 +addmap MAP28 +addmap MAP29 +addmap MAP30 +sv_maprotation 1 +sv_randommaprotation 0 +sv_hostemail '' +sv_hostname 'Doom 2 Vanilla Coop - wolf3d.linuxdn.org' +sv_website wolf3d.linuxdn.org +sv_password '' +sv_forcepassword 0 +sv_joinpassword '' +sv_forcejoinpassword 0 +sv_rconpassword '' +sv_broadcast 1 +sv_updatemaster 1 +sv_maxclients 12 +sv_maxplayers 12 -upnp +dmflags 1487552512 +dmflags2 0 +zadmflags 80 +compatflags 0 +compatflags2 0 +zacompatflags 0 +lmsallowedweapons 0 +lmsspectatorsettings 0 +sv_afk2spec 0 +sv_coop_damagefactor 1 +sv_defaultdmflags 0 +sv_limitnumvotes 1 +sv_minvoters 1 +sv_nocallvote 0 +sv_nochangemapvote 0 +sv_noduellimitvote 1 +sv_nofraglimitvote 1 +sv_nokickvote 0 +sv_nomapvote 1 +sv_nopointlimitvote 1 +sv_notimelimitvote 1 +sv_nowinlimitvote 1 +sv_noforcespecvote 1 +sv_motd '===  wolf3d.linuxdn.org  === \n\n\nWELCOME to Classic Doom 2 Cooperative!\n\nPlease visit our website for stats, info, and more :: WOLF3D.LINUXDN.ORG\n\nThis server is completely self-hosted on a mighty Raspberry Pi!'

#${SERVER}  -iwad ${WAD_DIR}/doom2.wad -port 11666 +sv_maxlives 0 -host +alwaysapplydmflags 1 -skill 4 +cooperative 1 +map E1M1 +addmap E1M1 +addmap E1M2 +addmap E1M3 +addmap E1M4 +addmap E1M5 +addmap E1M6 +addmap E1M7 +addmap E1M8 +addmap E2M1 +addmap E2M2 +addmap E2M3 +addmap E2M4 +addmap E2M5 +addmap E2M6 +addmap E2M7 +addmap E2M8 +addmap E3M1 +addmap E3M2 +addmap E3M3 +addmap E3M4 +addmap E3M5 +addmap E3M6 +addmap E3M7 +addmap E3M8 +addmap E4M1 +addmap E4M2 +addmap E4M3 +addmap E4M4 +addmap E4M5 +addmap E4M6 +addmap E4M7 +addmap E4M8 +sv_maprotation 1 +sv_randommaprotation 0 +sv_motd '' +sv_hostemail '' +sv_hostname 'Ultimate Doom Vanilla Coop - Ultra-violence difficulty - wolf3d.linuxdn.org' +sv_website wolf3d.linuxdn.org +sv_password '' +sv_forcepassword 0 +sv_joinpassword '' +sv_forcejoinpassword 0 +sv_rconpassword '' +sv_broadcast 1 +sv_updatemaster 1 +sv_maxclients 12 +sv_maxplayers 12 +dmflags 23134212 +dmflags2 64 +zadmflags 1108 +compatflags 0 +compatflags2 0 +zacompatflags 33554432 +lmsallowedweapons 0 +lmsspectatorsettings 3 +sv_afk2spec 0 +sv_coop_damagefactor 1 +sv_defaultdmflags 0




