#!/bin/bash

SERVER=~/bin/zandronum-server
WAD_DIR=~/wolf3d_server

# NOTE: be sure to start this script after invoking "screen -S wolf3d -L", as that will log the output


# Wolf3d mod for Doom, skill 1 (I'm too young to die)
# for TESTING ONLY:
#${SERVER}  -iwad ${WAD_DIR}/wolf3d.wad -file ${WAD_DIR}/stwolf3dv4.pk3 -port 10666 +sv_maxlives 0 -host +alwaysapplydmflags 1 -skill 1 +cooperative 1 +map e1m1 +addmap e1m1 +addmap E1M2 +addmap E1M3 +addmap E1M4 +addmap E1M5 +addmap E1M6 +addmap E1M7 +addmap E1M8 +addmap E1M9 +addmap E2M1 +addmap E2M2 +addmap E2M3 +addmap E2M4 +addmap E2M5 +addmap E2M6 +addmap E2M7 +addmap E2M8 +addmap E2M9 +addmap E3M1 +addmap E3M2 +addmap E3M3 +addmap E3M4 +addmap E3M5 +addmap E3M6 +addmap E3M7 +addmap E3M8 +addmap E3M9 +addmap E4M1 +addmap E4M2 +addmap E4M3 +addmap E4M4 +addmap E4M5 +addmap E4M6 +addmap E4M7 +addmap E4M8 +addmap E4M9 +addmap e5m1 +addmap E5M2 +addmap E5M3 +addmap E5M4 +addmap E5M5 +addmap E5M6 +addmap E5M7 +addmap E5M8 +addmap E5M9 +addmap E6M1 +addmap E6M2 +addmap E6M3 +addmap E6M4 +addmap E6M5 +addmap E6M6 +addmap E6M7 +addmap E6M8 +addmap E6M9 +addmap E7M1 +addmap E7M2 +addmap E7M3 +addmap E7M4 +addmap E7M5 +addmap E7M6 +addmap E7M7 +addmap E7M8 +addmap E7M9 +addmap E7M10 +addmap E7M11 +addmap E7M12 +addmap E7M13 +addmap E7M14 +addmap E7M15 +addmap E7M16 +addmap E7M17 +addmap E7M18 +addmap E7M19 +addmap E7M20 +addmap E7M21 +addmap e8m1 +addmap e8m2 +addmap e8m3 +addmap e8m4 +addmap e8m5 +addmap e8m6 +addmap e8m7 +addmap e8m8 +addmap e8m9 +addmap e8m10 +addmap e8m11 +addmap e8m12 +addmap e8m13 +addmap e8m14 +addmap e8m15 +addmap e8m16 +addmap e8m17 +addmap e8m18 +addmap e8m19 +addmap e8m20 +addmap e8m21 +addmap e8m22 +addmap e8m23 +addmap e8m24 +addmap e8m25 +addmap e8m26 +addmap e8m27 +addmap e8m28 +addmap e8m29 +addmap e8m30 +sv_maprotation 1 +sv_randommaprotation 0 +sv_motd '' +sv_hostemail '' +sv_hostname 'Wolfenstein 3d Throw-back (testing)' +sv_website grube.linuxdn.org +sv_password '' +sv_forcepassword 0 +sv_joinpassword '' +sv_forcejoinpassword 0 +sv_rconpassword '' +sv_broadcast 1 +sv_updatemaster 1 +sv_maxclients 8 +sv_maxplayers 8 +dmflags 9060356 +dmflags2 64 +zadmflags 1108 +compatflags 0 +zacompatflags 33554432 +lmsallowedweapons 0 +lmsspectatorsettings 3 +sv_afk2spec 0 +sv_coop_damagefactor 1 +sv_defaultdmflags 0


# Here's the real server boot, with these options:
# -Nightmare difficulty (skill 5)
# -no monster respawn
# -no item respawn
# -players get infinite lives
# -keys shared between players
# -no jumping/crouching
# -players can walk through each other
#
# -E7M1 (Spear of Destiny lvl 1) is disabled, as the spawns at the start are broken
# -Secret levels (e1-6m0 , e7m19 , e7m20) are disabled - you have to get to them the hard way!
# -E8M2 Disabled - crazy secrets need to be discovered just to beat level
# -E8M6 is Disabled - it's bugged - flag in the way of the exit elevator
# -NOTE: need doom.wad or doom2.wad to stop the nagging "uknown texture" - wolf3d.wad won't work


${SERVER}  -iwad ${WAD_DIR}/doom.wad -file ${WAD_DIR}/stwolf3dv4.pk3 -port 10666 +sv_maxlives 0 -host +alwaysapplydmflags 1 -skill 5 +cooperative 1 +map e1m1 +addmap e1m1 +addmap E1M2 +addmap E1M3 +addmap E1M4 +addmap E1M5 +addmap E1M6 +addmap E1M7 +addmap E1M8 +addmap E1M9 +addmap E2M1 +addmap E2M2 +addmap E2M3 +addmap E2M4 +addmap E2M5 +addmap E2M6 +addmap E2M7 +addmap E2M8 +addmap E2M9 +addmap E3M1 +addmap E3M2 +addmap E3M3 +addmap E3M4 +addmap E3M5 +addmap E3M6 +addmap E3M7 +addmap E3M8 +addmap E3M9 +addmap E4M1 +addmap E4M2 +addmap E4M3 +addmap E4M4 +addmap E4M5 +addmap E4M6 +addmap E4M7 +addmap E4M8 +addmap E4M9 +addmap e5m1 +addmap E5M2 +addmap E5M3 +addmap E5M4 +addmap E5M5 +addmap E5M6 +addmap E5M7 +addmap E5M8 +addmap E5M9 +addmap E6M1 +addmap E6M2 +addmap E6M3 +addmap E6M4 +addmap E6M5 +addmap E6M6 +addmap E6M7 +addmap E6M8 +addmap E6M9 +addmap E7M2 +addmap E7M3 +addmap E7M4 +addmap E7M5 +addmap E7M6 +addmap E7M7 +addmap E7M8 +addmap E7M9 +addmap E7M10 +addmap E7M11 +addmap E7M12 +addmap E7M13 +addmap E7M14 +addmap E7M15 +addmap E7M16 +addmap E7M17 +addmap E7M18 +addmap E7M21 +addmap e8m1 +addmap e8m3 +addmap e8m4 +addmap e8m5 +addmap e8m7 +addmap e8m8 +addmap e8m9 +addmap e8m10 +addmap e8m11 +addmap e8m12 +addmap e8m13 +addmap e8m14 +addmap e8m15 +addmap e8m16 +addmap e8m17 +addmap e8m18 +addmap e8m19 +addmap e8m20 +addmap e8m21 +addmap e8m22 +addmap e8m23 +addmap e8m24 +addmap e8m25 +addmap e8m26 +addmap e8m27 +addmap e8m28 +addmap e8m29 +addmap e8m30 +sv_maprotation 1 +sv_randommaprotation 0 +sv_motd '' +sv_hostemail '' +sv_hostname 'Wolfenstein 3D Co-op - wolf3d.linuxdn.org' +sv_website wolf3d.linuxdn.org +sv_password '' +sv_forcepassword 0 +sv_joinpassword '' +sv_forcejoinpassword 0 +sv_rconpassword '' +sv_broadcast 1 +sv_updatemaster 1 +sv_maxclients 16 +sv_maxplayers 16 +dmflags 4784132 +dmflags2 64 +zadmflags 1108 +compatflags 0 +compatflags2 0 +zacompatflags 33554432 +lmsallowedweapons 0 +lmsspectatorsettings 3 +sv_afk2spec 0 +sv_coop_damagefactor 1 +sv_defaultdmflags 0 +sv_motd '===  wolf3d.linuxdn.org  === \n\n\nWELCOME to Wolf3D Cooperative!\n\nPlease visit our website for stats, info, and more :: WOLF3D.LINUXDN.ORG\n\nThis server is completely self-hosted on a mighty Raspberry Pi!'




