#!/bin/bash

# Simple script to run a playerinfo command on server and capture the output

# Need to pass 2 arguments:
# 1: screen name (doom, wolf3d, etc.)
# 2: screen log file (location of screenlog.0)
SCREENSESSION=$1
SCREENLOG=$2
SERVER_INFO=/srv/www/tmp/wolf3d_server_info.txt

COUNT_FILE=/srv/wolf3d/stats/${SCREENSESSION}.txt


echo "
Generated on: <b>`date`</b>

<p>

"

# check to see if the desired screen exists.  
# If we can't find it, then exit and make that known
screenCount=`screen -ls | grep "\.${SCREENSESSION}" | wc -l`

if [[ "$screenCount" -lt "1" ]]; then
	echo "<b>ERROR: Cannot find server screen session labeled:  ${SCREENSESSION} </b> .  Stats are not available.</b>"
	exit 1
fi




# Send a bunch of spaces to screen
screen -S ${SCREENSESSION} -X stuff "^M^M^M^M^M^M^M^M^M^M"
screen -S ${SCREENSESSION} -X stuff "PLAYERINFO^M"
screen -S ${SCREENSESSION} -X logfile flush 1

sleep 2

#PLAYERS=`tail -10 ${SCREENLOG} | grep -v PLAYERINFO | tr '\r' '\n' | sed '/^$/d'`
PLAYERS=`tail -10 ${SCREENLOG} | grep "IP"  |  sed '/^[[:space:]]*$/d'`
#| tr '\r' '\n'`


# Get current map:
MAP=`tac ${SCREENLOG} | grep '\*\*\*' | head -1 | tr -d '*' | tr -d '\r'`

#echo "PLAYERS == `echo "${PLAYERS}" | cat -v`"

#echo "MAP == `echo "${MAP}" | cat -v`"

echo "
Current Map :: <b>${MAP}</b>

<p>

<b>Player List:</b>
<table border=1>
"


while IFS= read -r line ; do 
	echo "<tr><td>
${line}
</td></tr>
"
done <<< "${PLAYERS}"


echo "</table>
"




# Get a player count.  If we have one or more players on, log the number
player_count=`echo "${PLAYERS}" | grep "IP" | wc -l`

if [ "${player_count}" -gt "0" ]; then
	echo "`date +"%Y-%m-%d  %H:%M"` , ${player_count}" >> ${COUNT_FILE}
fi



#echo "${PLAYERS}" | cat -v
